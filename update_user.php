<?php 
	include './config/connection.php';

	if (isset($_POST['submit'])) {
		$gotoPage = "users";
		$message = "";

		$hiddenId = $_POST['hidden_id'];
		
		$userName = trim($_POST['user_name']);
		$password = $_POST['password'];
		$email = trim($_POST['user_email']);
		$displayName = trim($_POST['display_name']);

	$displayName = ucwords(strtolower($displayName));
		try {
			$con->beginTransaction();

			if ($userName != '' 
				&& $email != '' && $displayName != '') {
				$query = "UPDATE `users` SET 
			`user_name`='$userName', 
			`email`='$email', 
			`display_name`='$displayName' 
			WHERE `id` = '$hiddenId';";
			$stmntUser = $con->prepare($query);
			$stmntUser->execute();

			if($password != '') {
				$encryptedPassword = md5($password);
				$query = "UPDATE `users` SET 
			`password`='$encryptedPassword' 
			WHERE `id` = '$hiddenId';";
			$stmntUser = $con->prepare($query);
			$stmntUser->execute();
			}
			}

			$con->commit();

			$message = 'User has been Updated successfully.';
		}
		catch(PDOException $e){
			
			$con->rollback();
			
			echo $e->getMessage();
			echo $e->getTraceAsString();
			exit;
		}
		header("location:congratulation?goto_page=".$gotoPage."&message=".$message);
			exit;
	}

	$id = $_GET['id'];
	$query = "SELECT * FROM `users`
	where `id` = '$id';";
	$stmtSearch = $con->prepare($query);
	$stmtSearch->execute();
	$row = $stmtSearch->fetch(PDO::FETCH_ASSOC);
	
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include './config/site-css.php';?>

</head>
<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php include './config/top-menu.php';?>

		<?php include './config/sidebar.php';?>

		<div class="content-wrapper">
			
			<!-- Main content -->
			<section class="content">
				<div class="container-fluid">
					<div class="row">
			<div class="col-md-12">
			<div class="card card-primary">
				<div class="card-header">
					<h3 class="card-title">Update User</h3>
				</div>
	<div class="card-body">
		<form method="POST">

			<input type="hidden" name="hidden_id" value="<?php echo $id;?>">
			<div class="row">
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="form-group">
						<label>User Name</label>
						<input type="text" name="user_name" class="form-control is_valid" placeholder="Enter User Name" required="required" value="<?php echo $row['user_name']; ?>" id="">
					</div>	

					
				</div>
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="form-group">
						<label>Password</label>
						<input type="password" name="password" class="form-control is_valid" 
						id="password">
					</div>
				</div>
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="form-group">
						<label>Email</label>
						<input type="text" name="user_email" class="form-control is_valid" value="<?php echo $row['email']; ?>" placeholder="abc@gmail.com" required="required" id="">
					</div>
				</div>
				
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="form-group">
						<label>Display Name</label>
						<input type="text" name="display_name" class="form-control is_valid" value="<?php echo $row['display_name']; ?>" placeholder="Display Name" required="required" id="">
					</div>
				</div>
				<div class="col-6"></div>
				<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-2">
					<label>&nbsp;</label>
					<button type="submit" name="submit" class="btn btn-success form-control btn-block">Save</button>
				</div>
			</div>
		</form>
	</div>
			</div>
			</div>
		</div>
				</div>
			</section>
		</div>


		<?php include './config/footer.php';?>
	</div>

	<?php include './config/site-js.php';?>
	<script type="text/javascript">

	</script>
</body>
</html>
