<?php 
include 'config/connection.php';
$id = $_GET['id'];

if (isset($_POST['submit'])) {
  $gotoPage = "categories";
  $message = "";

  $category_name = trim($_POST['category']);
  
  $category_name = ucwords(strtolower($category_name));


  try{
    $con->beginTransaction();

    if($category_name !== '' ) {

      $query = "UPDATE `categories` SET `category`='$category_name' WHERE `id` = $id;";
      $stmt = $con->prepare($query);
      $stmt->execute();
    }                  

    $con->commit();
    $message = "the data hes been Updated successfully";
  }

  catch(PDOException $ex) {
    $con->rollback();
    echo $ex->getMessage();
    echo $ex->getTraceAsString();
    exit;
  }
  header("location:congratulation?goto_page=".$gotoPage."&message=".$message);


}
$query = "SELECT * FROM `categories` where id = $id ;";
$stmt = $con->prepare($query);
$stmt->execute();

$row = $stmt->fetch(PDO::FETCH_ASSOC); 
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include './config/site-css.php';?>

</head>
<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php include './config/top-menu.php';?>

		<?php include './config/sidebar.php';?>

		<div class="content-wrapper">
			
			<!-- Main content -->
			<section class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
							<div class="card card-primary">
								<div class="card-header">
									<h3 class="card-title">Update Category</h3>
								</div>
								<!-- /.card-header -->
								<!-- form start -->
								<form id="quickForm" method="post">
									<div class="card-body">
										<div class="form-group">
											<label for="category">Category</label>
											<input type="text" name="category"
											value="<?php echo $row['category']?>" class="form-control" id="category" placeholder="Enter category">
										</div>

									</div>
									<!-- /.card-body -->
									<div class="card-footer">
										<button name="submit" type="submit" class="btn btn-primary">Submit</button>
									</div>
								</form>
							</div>
							<!-- /.card -->
						</div>
					</div>
				</div><!-- /.container-fluid -->
			</section>

			
		</div>


		<?php include './config/footer.php';?>
	</div>

	<?php include './config/site-js.php';?>
	</body>
</html>
