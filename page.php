<!DOCTYPE html>
<html lang="en">
<head>
	<?php include './config/site-css.php';?>

</head>
<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php include './config/top-menu.php';?>

		<?php include './config/sidebar.php';?>

		<div class="content-wrapper">
			<section class="content-header">
				<div class="container-fluid">
					<div class="row mb-2">
						<div class="col-sm-6">
							<h1>Page Title</h1>
						</div>
					</div>
				</div><!-- /.container-fluid -->
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="card card-primary">
								<div class="card-header">
									<h3 class="card-title">card title</h3>
								</div>
								<!-- /.card-header -->
								<!-- form start -->
								<form id="quickForm">
									<div class="card-body">
										<div class="form-group">
											<label for="exampleInputEmail1">Email address</label>
											<input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
										</div>
										<div class="form-group mb-0">
											<div class="custom-control custom-checkbox">
												<input type="checkbox" name="terms" class="custom-control-input" id="exampleCheck1">
												<label class="custom-control-label" for="exampleCheck1">I agree to the <a href="#">terms of service</a>.</label>
											</div>
										</div>
									</div>
									<!-- /.card-body -->
									<div class="card-footer">
										<button type="submit" class="btn btn-primary">Submit</button>
									</div>
								</form>
							</div>
							<!-- /.card -->
						</div>
					</div>
				</div><!-- /.container-fluid -->
			</section>
		</div>


		<?php include './config/footer.php';?>
	</div>

	<?php include './config/site-js.php';?>
	<script type="text/javascript">

	</script>
</body>
</html>
