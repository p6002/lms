<?php 
include 'config/connection.php';
if(isset($_POST['submit'])) {
$categoryName = trim($_POST['category_name']);

$query1 = "SELECT * FROM `categories` where `category` = '$categoryName';";

$stmtCategory = $con->prepare($query1);
$stmtCategory->execute();
 
 $row = $stmtCategory->fetch(PDO::FETCH_ASSOC);

 $dbCategoryName = $row['category'];

  $gotoPage = "categories";
  $message = "";

  
  $categoryName = ucwords(strtolower($categoryName));
  
  try {
    $con->beginTransaction();

    if($categoryName != '' && $categoryName != $dbCategoryName) {
      $query = "INSERT INTO `categories`(`category`) VALUES ('$categoryName');";

      $stmt = $con->prepare($query);
      $stmt->execute();
    $message = 'Category has been registered successfully.';


    } else {
    	$message = 'Category already exsist';
    }

    $con->commit();


  } catch(PDOException $ex) {
    $con->rollback();
    echo $ex->getMessage();
    echo $ex->getTraceAsString();
    exit;
  }
 echo $message;
 
  header("location:congratulation?goto_page=".$gotoPage."&message=".$message);
  exit;
}


$query = "SELECT * FROM `categories` ORDER BY `category` ASC;";
$stmtSuppliers = $con->prepare($query);
$stmtSuppliers->execute();

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php include './config/site-css.php';?>

</head>
<body class="hold-transition sidebar-mini">
  <div class="wrapper">

    <?php
    include "config/top-menu.php";

    include "config/sidebar.php";

    ?>
    <!-- Main Sidebar Container -->
    <div class="clearfix">&nbsp;</div>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

      <!-- Main content -->
      <div class="container-fluid">
        <div class="row">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card ">
              <div class="card-header bg-primary text-white">Add New Category</div>
              <div class="card-body">

                <form method="post">
                  <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                      <label for="category_name">Category Name</label>
                      <input type="text" id="category_name" placeholder="Enter Category Name" name="category_name" required="required" class="form-control is_valid">
                    </div>


                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                      <label for="">&nbsp;</label>
                      <button type="submit" id="submit" 
                      name="submit" class="btn btn-primary  btn-block">Save</button>
                    </div>

                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>

        <div class="clearfix">&nbsp;</div>
        <div class="clearfix">&nbsp;</div>

        <div class="row">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
              <div class="card-header bg-primary text-white">All Categries
              </div>
              <div class="card-body">

                <div class="row">
                  <div class="col-xl-1 col-lg-2 col-md-1 col-sm-3 col-12">
                    <a title="print categories in pdf" class="btn btn-success" target="_blank" href="print_categories">
                      <i class="fa fa-print"></i>
                    </a>
                  </div>
                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 table-responsive">
                    <table class="table table-bordered" id="example1">
                      <thead>
                        <tr>
                          <th>S.No</th>
                          <th>Category Name</th>
                          <th>Action</th>
                        </tr>
                      </thead>

                      <tbody>
                        <?php 
                        $counter = 0;
                        while($row = $stmtSuppliers->fetch(PDO::FETCH_ASSOC)) {
                          $counter++;
                          ?>
                          <tr>
                            <td><?php echo $counter;?></td>
                            <td><?php echo $row['category'];?></td>
                            <td>

                              <a href="update_categories?id=<?php echo $row['id'];?>" class="btn btn-primary"><i class="fas fa-edit"></i></a>
                            </td>
                          </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>      
    </div>
  </section>

  <?php
  include "config/footer.php";
  ?>
</div>

<?php include './config/site-js.php';?>

<!-- Page specific script -->
<script>
  $(document).ready(function() {
    showMenuSelected("#mnu_books", "#mi_categories");
  });
</script>

</body>
</html>
