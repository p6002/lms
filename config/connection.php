<?php 
$host = "localhost";
$username = "root";
$password = "";

try {
  $con = new PDO("mysql:host=$host;dbname=library_management_system", 
  	$username, $password);

  $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

} catch(PDOException $e) {
 echo $e->getMessage();
 echo '<br />';
 echo $e->getTraceAsString();
 exit;
}

session_start();

?>