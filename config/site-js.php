<!-- jQuery -->
<script src="./assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="./assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Select2 -->
<script src="./assets/plugins/select2/js/select2.full.min.js"></script>

<!-- InputMask -->
<script src="./assets/plugins/moment/moment.min.js"></script>

<!-- date-range-picker -->
<script src="./assets/plugins/daterangepicker/daterangepicker.js"></script>

<!-- Tempusdominus Bootstrap 4 -->
<script src="./assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Bootstrap Switch -->

<!-- AdminLTE App -->
<script src="./assets/dist/js/adminlte.min.js"></script>

<script src="./assets/dist/js/messagebox.js"></script>


<?php 
$message = '';
if(isset($_SESSION['message'])) {
  $message = $_SESSION['message'];
  
  unset($_SESSION['message']);
}
?>
<script type="text/javascript">
  /* 
  view-source:https://www.jqueryscript.net/demo/jQuery-Plugin-To-Replace-Native-JS-Popup-Boxes-MessageBox/
  */
  var message = '<?php echo $message;?>';
  if(message !== '') {
    $.MessageBox(message);
 }


  $(document).ready(function() {
    $('.is_valid').blur(function() {
      var data = $(this).val().trim();
      $(this).val(data);
    });
  });

 
 $(function () {
      $('.select2').select2();
    });


function showMenuSelected(menuId, menuItemId) {
  $(menuId).addClass('menu-open');
  //jquery chaining
  $(menuId).children("a").first().addClass('active');

  $(menuItemId).addClass('active');

}

function showCustomMessage(message) {
    $.MessageBox(message);
}
</script>

