 <?php 
  $url = '';
  if(!(isset($_SESSION['user_id']))) {
    $url = 'index';
  }
  ?>
  <script>
    var url = '<?php echo $url;?>';
    if(url !== '') {
      location.replace(url);
    }
  </script>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <a href="" class="brand-link">
    <img src="./assets/dist/img/AdminLTELogo.png" 
    alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: 0.8">
    <span class="brand-text font-weight-light">LMS</span>
  </a>

  <div class="sidebar">
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="./assets/user_images/<?php echo $_SESSION['profile_picture'];?>" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block"><?php echo $_SESSION['display_name'];?></a>
      </div>
    </div>

    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item" id="mnu_dashboard">
            <a href="dashboard" class="nav-link" id="mi_dashboard">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>

          <li class="nav-item" id="mnu_books">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Books
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="categories" class="nav-link" id="mi_categories">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Categories</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="authors" class="nav-link" id="mi_authors">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Authors</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="books" class="nav-link" id="mi_books">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Books</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item" id="mnu_issue_return">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Book Issue / Return
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="issue_books" class="nav-link" id="mi_issue_book">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Issue Books</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pending_return_books" class="nav-link" 
                id="mi_pending_return">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Pending & Return</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item" id="mnu_reports">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-tree"></i>
              <p>
                Reports
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="print_books_report" id="mi_reports_books" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Books Reports</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="print_pending_return_reports" id="mi_reports_members" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Member Reports</p>
                </a>
              </li>
              
            </ul>
          </li>

          <li class="nav-item" id="mnu_settings">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Settings
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="members" class="nav-link" id="mi_members">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Members</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="users" class="nav-link" id="mi_users">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Users</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item">
            <a href="logout" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                Logout
              </p>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </aside>