<?php 


  function getAllcategory ($con, $categoryId = 0) {
      $query = "SELECT `id`, `category`
      FROM `categories` 
        ORDER BY `category` ASC;";     
$stmtSuppliers = $con->prepare($query);
$stmtSuppliers->execute();

  $data = "<option  value=''>Select Category</option>";
  while($row = $stmtSuppliers->fetch(PDO::FETCH_ASSOC)) {
    if($categoryId == $row['id']) {
          $data = $data. "<option selected='selected' value='".$row['id']."'>".$row['category']."</option>";
    } else {
          $data = $data. "<option value='".$row['id']."'>".$row['category']."</option>";
    }

  
}
  return $data;

}


 function getAllAuthor ($con, $authorId = 0) {
     
      $query = "SELECT `id`, `author_name`, `email`, `contact_number` FROM `authors` ORDER BY `author_name`;";     
$stmtSuppliers = $con->prepare($query);
$stmtSuppliers->execute();

  $data = "<option value=''>Select Author</option>";
  while($row = $stmtSuppliers->fetch(PDO::FETCH_ASSOC)) {
    if($authorId == $row['id']) {
    $data = $data. "<option selected='selected' value='".$row['id']."'>".$row['author_name']."</option>";
    } else {
    $data = $data. "<option value='".$row['id']."'>".$row['author_name']."</option>";      
    }

  
}
  return $data;

}

function getAllMembers ($con, $memberId = 0) {
     
      $query = "SELECT `id`, `member_name`, `cnic_number`, `contact_number`, `address` FROM `members`;";     
$stmtMembers = $con->prepare($query);
$stmtMembers->execute();

  $data = "<option value='' data-select2-id=''>Select Member</option>";
  while($row = $stmtMembers->fetch(PDO::FETCH_ASSOC)) {
    if($memberId == $row['id']) {
    $data = $data. "<option selected='selected' value='".$row['id']."'>".$row['member_name']." "."(".$row['cnic_number'].")"."</option>";
    } else {
    $data = $data. "<option value='".$row['id']."'>".$row['member_name']." "."("."CNIC: ".$row['cnic_number'].")"."</option>";      
    }

  
}
  return $data;

}




function getAllBooks ($con, $bookId = 0) {
     
      $query = "SELECT `b`.`id`, `b`.`book_name`, 
      `b`.`edition`, `c`.`category`, 
      `a`.`author_name`  
      FROM `books` AS `b`, `categories` AS `c`, 
      `authors` AS `a` 
      WHERE `b`.`category_id` = `c`.`id` and 
      `b`.`author_id` = `a`.`id` 
      ORDER BY `book_name` ASC;";
$stmtBooks = $con->prepare($query);
$stmtBooks->execute();

  $data = "<option value=''>Select Book</option>";
  while($row = $stmtBooks->fetch(PDO::FETCH_ASSOC)) {
    if($bookId == $row['id']) {
    $data = $data. "<option selected = 'selected' value='".$row['id']."'>".$row['book_name']." "."(".$row['edition']." "."Edition".")"." "."(".$row['category'].")"." "."("."By"." ".$row['author_name'].")"."</option>";
    } else {
    $data = $data. "<option value='".$row['id']."'>".$row['book_name']." "."(".$row['edition']." "."Edition".")"." "."(".$row['category'].")"." "."("."By"." ".$row['author_name'].")"."</option>";      
    }
}
  return $data;
}

/* lcd's   -> to change to lcd\'s  */
function replaceSingleQuote($data) {
  $data = str_replace("'", "\'", $data);
  return $data;
}

function changeDateToMysql($date) {
  $dateArr = explode("/", $date);
  // 02/03/2022 /m/d/Y
  $formatedDate = $dateArr[2].'-'.$dateArr[0].'-'.$dateArr[1];
  return $formatedDate;
}
?>
