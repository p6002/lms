<?php 
	include './config/connection.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include './config/site-css.php';?>

</head>
<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php include './config/top-menu.php';?>

		<?php include './config/sidebar.php';?>

		<div class="content-wrapper">
		
<div class="row mb-2">
						
					</div>
			<!-- Main content -->
			<section class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="card card-primary">
								<div class="card-header">
									<h3 class="card-title">Books Report</h3>
								</div>
								<!-- /.card-header -->
								<!-- form start -->
									<div class="card-body">
										<div class="row">
											<div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-xs-12">
												<a title="print Categories in pdf" href="print_categories"
												target="_blank" class="btn btn-success btn-block"><i class="fas fa-print"></i>
											&nbsp;&nbsp; Print Categories</a>
											</div>
											<div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-xs-12">
												<a title="print Authors in pdf" href="print_authors" target="_blank" class="btn btn-success btn-block"><i class="fas fa-print"></i>
												&nbsp;&nbsp;Print Authors</a>
											</div>

											<div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-xs-12">
												<a title="print books in pdf" href="print_books" target="_blank" class="btn btn-success btn-block"><i class="fas fa-print"></i>
												&nbsp;&nbsp;Print Books</a>
											</div>
										</div>
									</div>
								
							
								</div>
						
						</div>
					</div>
				</div><!-- /.container-fluid -->
			</section>
		</div>


		<?php include './config/footer.php';?>
	</div>

	<?php include './config/site-js.php';?>
	<script type="text/javascript">
		$(document).ready(function() {
    showMenuSelected("#mnu_reports", "#mi_reports_books");
  });
		</script>
</body>
</html>
