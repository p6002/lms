<?php 
include './config/connection.php';
include "./common_service/common_functions.php";
$id = $_GET['id'];

if (isset($_POST['submit'])) {
  $gotoPage = "books";
  $message = "";

  $category = $_POST['category_name'];
  $author = $_POST['author_name'];
  $bookName = trim($_POST['book_name']);
  $number_of_pages = trim($_POST['number_of_pages']);
  $purchase_price = trim($_POST['purchase_price']);
  $edition = trim($_POST['edition']);
  $quantity = trim($_POST['quantity']);

  $bookName = ucwords(strtolower($bookName));

  try{
    $con->beginTransaction();

    if($bookName !== '' && $edition !== '' && $number_of_pages > 0 && $purchase_price > 0 && $quantity > 0 ) {

      $query = "UPDATE `books` 
      SET `book_name`='$bookName',
      `number_of_page`= $number_of_pages,
      `purchase_price`= $purchase_price,
      `author_id`= $author,
      `edition`='$edition',
      `quantity`= $quantity,
      `category_id`= $category 
      WHERE `id` = $id;";
      $stmt = $con->prepare($query);
      $stmt->execute();
    }                  

    $con->commit();
    $message = "the data hes been Updated successfully";
  }

  catch(PDOException $ex) {
    $con->rollback();
    echo $ex->getMessage();
    echo $ex->getTraceAsString();
    exit;
  }
  header("location:congratulation?goto_page=".$gotoPage."&message=".$message);
exit;
}

$query = "SELECT `b`.`id`, `b`.`book_name`, `b`.`number_of_page`, `b`.`purchase_price`, `b`.`author_id`, `b`.`edition`, `b`.`quantity`, `b`.`category_id`, `c`.`category`, `a`.`author_name` FROM `books` AS `b`, `categories` AS `c`, `authors` AS `a` 
WHERE `c`.`id` = `b`.`category_id` && `a`.`id` = `b`.`author_id` && `b`.`id` = $id;";
$stmt = $con->prepare($query);
$stmt->execute();
$row = $stmt->fetch(PDO::FETCH_ASSOC);
$categoryId = $row['category_id'];
$authorId = $row['author_id'];

$category = getAllcategory($con, $categoryId);
$author = getAllAuthor($con, $authorId);

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>L-M-S | Books</title>
<?php
include "./config/site-css.php";

?>
</head>
<body class="hold-transition sidebar-mini">
  <div class="wrapper">
    <!-- Navbar -->
    <?php
    include "./config/top-menu.php";

  // <!-- /.navbar -->
    include "./config/sidebar.php";

    ?>
    <!-- Main Sidebar Container -->
    <div class="clearfix">&nbsp;</div>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

           <div class="row">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card ">

              <div class="card-header bg-primary text-white">Update Book</div>
              <div class="card-body">
  
                <form method="POST">
                  <div class="row">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                      <label for="category">Select Category</label>
                      <select  id="category_name" 
                      name="category_name" required="required" class="form-control select2 is_valid">
                      <?php echo $category;?>
                    </select>
                  </div>

                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                    <label for="author_name">Select Author
                    </label>
                    <select  id="author_name"  name="author_name" required="required" class="form-control is_valid select2">
                      <?php echo $author;?>
                    </select> 
                  </div>

                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                    <label for="book_name">Book Name</label>
                    <input type="text" value="<?php echo $row['book_name'];?>" id="book_name" name="book_name" required="required" class="form-control is_valid">
                  </div>

                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                    <label for="number_of_pages">Number Of Pages</label>
                    <input type="text" value="<?php echo $row['number_of_page'];?>" id="number_of_pages" name="number_of_pages" required="required" class="form-control is_valid">
                  </div>

                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                    <label for="purchase_price">Purchase Price</label>
                    <input type="text" value="<?php echo $row['purchase_price'];?>" required="required" id="purchase_price" 
                    name="purchase_price" class="form-control is_valid">
                  </div>
                  <div class="col-xl-3 col-lg-2 col-md-2 col-sm-6 col-12">
                    <label for="edition">Edition</label>
                    <input type="text" value="<?php echo $row['edition'];?>" required="required" id="edition" 
                    name="edition" class="form-control is_valid">
                  </div>

                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                    <label for="qantity">Quantity</label>
                    <input type="text" value="<?php echo $row['quantity'];?>" required="required" id="qantity" 
                    name="quantity" class="form-control is_valid">
                  </div>

                  <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-12">
                    <label for="">&nbsp;</label>
                    <button type="submit" id="submit" 
                    name="submit" class="btn btn-primary  btn-block">Save</button>
                  </div>
                </div>

              </form>
  
            </div>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php
include "config/footer.php";
?>
</div>
<?php
include "config/site-js.php";
?>
 
</body>
</html>
