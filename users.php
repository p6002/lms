<?php 
	include './config/connection.php';
	
//https://www.w3schools.com/php/php_file_upload.asp
	
	if (isset($_POST['submit'])) {
		$gotoPage = "users";
		$message = "";

		$userName = trim($_POST['user_name']);
		$password = md5($_POST['password']);
		$email = trim($_POST['user_email']);
		$displayName = trim($_POST['display_name']);

	$displayName = ucwords(strtolower($displayName));

		try{
			$con->beginTransaction();
			if ($userName != '' && $password != '' 
				&& $email != '' && $displayName != '') {

				$profilePicture = NULL;
				if(isset($_FILES['profile_picture'])) {
					$target_dir = "assets/user_images/";
					$target_file = $target_dir . basename($_FILES["profile_picture"]["name"]);

					move_uploaded_file($_FILES["profile_picture"]["tmp_name"], $target_file);
					$profilePicture = basename($_FILES["profile_picture"]["name"]);
				}


				$query = "INSERT INTO `users`(`user_name`, `password`, `email`, `display_name`) VALUES('$userName', '$password', '$email', '$displayName');";
				if($profilePicture != NULL) {
					$query = "INSERT INTO `users`(`user_name`, `password`, `email`, 
					`display_name`, `profile_picture`) VALUES('$userName', '$password', '$email', '$displayName', '$profilePicture');";
				}
			$stmntUser = $con->prepare($query);
			$stmntUser->execute();

			
			}
			$con->commit();
			$message = 'User has been registered successfully.';
		}
		catch(PDOException $e){
			$con->rollback();
			echo $e->getMessage();
			echo $e->getTraceAsString();
		}
		header("location:congratulation?goto_page=".$gotoPage."&message=".$message);
			exit;
	}

	$query = "SELECT * FROM `users`
	ORDER BY `is_active` desc;";
	$stmntUser = $con->prepare($query);
	$stmntUser->execute();
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include './config/site-css.php';?>

</head>
<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php include './config/top-menu.php';?>

		<?php include './config/sidebar.php';?>

		<div class="content-wrapper">
			<section class="content-header">
				<div class="container-fluid">
					<div class="row mb-2">
						<div class="col-sm-6">
							<h1>Add New User</h1>
						</div>
					</div>
				</div><!-- /.container-fluid -->
			</section>

			<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
			<div class="card card-primary">
				<div class="card-header">
					<h3 class="card-title">Add User</h3>
				</div>
	<div class="card-body">
		<form method="POST" enctype="multipart/form-data">
			<div class="row">
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="form-group">
						<label>User Name</label>
						<input type="text" name="user_name" class="form-control is_valid" placeholder="Enter User Name" required="required" value="" id="">
					</div>	

					
				</div>
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="form-group">
						<label for="password">Password</label>
						<input type="password" name="password" class="form-control is_valid" value="" placeholder="Password" required="required" id="password">
					</div>
				</div>

				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="form-group">
						<label>Email</label>
						<input type="text" name="user_email" class="form-control is_valid" value="" placeholder="abc@gmail.com" required="required" id="">
					</div>
				</div>
				
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="form-group">
						<label>Display Name</label>
						<input type="text" name="display_name" class="form-control is_valid" value="" placeholder="Display Name" required="required" id="display_name">
					</div>
				</div>

				<div class="col-xl-4 col-lg-4 col-md-3 col-sm-6 col-xs-12">
					<div class="form-group">
						<label>Picture</label>
						<input type="file" name="profile_picture" class="form-control"  id="profile_picture">
					</div>
				</div>

				
				<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-2">
					<label>&nbsp;</label>
					<button type="submit" name="submit" class="btn btn-success form-control btn-block">Save</button>
				</div>
			</div>
		</form>
	</div>
			</div>
							<!-- /.card -->
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>
<div class="clearfix">&nbsp;</div>
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
			<div class="card card-primary">
				<div class="card-header">
					<h3 class="card-title">All Users</h3>
				</div>
	<div class="card-body">
		<div class="table-responsive col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<table class="table table-bordered table-hover" id="example1">
				<thead>
					<tr>
						<th>S.No</th>
						<th>UserName</th>
						<th>Email</th>
						<th>DisplayName</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					$counter = 0;
					$status = '';
					$cssClass = 'btn-success status';
					while ($row = $stmntUser->fetch(PDO::FETCH_ASSOC)) {
						$cssClass = 'btn-success status';
						$counter++;
						$status = 'Active';
						  if ($row['is_active'] == 0) {
						  	$status = 'Disabled';
						  	$cssClass = 'btn-danger status'; 
						  }
					 ?>
					 <tr>
					 	<td><?php echo $counter; ?></td>
					 	<td><?php echo $row['user_name']; ?></td>
					 	<td><?php echo $row['email']; ?></td>
					 	<td><?php echo $row['display_name']; ?></td>
					 	<td><span class="btn <?php echo $cssClass;?>"><?php echo $status;?></span></td>
					 	<td>
					 		<?php 
					 			$lockClass = 'fa-lock';
					 			$redGreen = 'btn-success';
					 			if ($row['is_active'] == 0) {
					 				$lockClass = 'fa-lock-open';
					 				$redGreen = 'btn-danger';
					 			}
					 		 ?>
					 		<a href="block_unblock_user?id=<?php echo $row['id'];?>&is_active=<?php echo $row['is_active']; ?>" class="btn <?php echo $redGreen; ?>"><i class="fas <?php echo $lockClass; ?>"></i></a>
					 		<a href="update_user?id=<?php echo $row['id'];?>" class="btn btn-success"><i class="fas fa-edit"></i></a>
					 	</td>
					 </tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
			</div>
							<!-- /.card -->
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>
</div>

		<?php include './config/footer.php';?>
	</div>

	<?php include './config/site-js.php';?>
<script type="text/javascript">
  $(document).ready(function() {
    showMenuSelected("#mnu_settings", "#mi_users");
  });
</script>
</body>
</html>
