<?php
include './config/connection.php';
include "./common_service/common_functions.php";

if (isset($_POST['submit'])) {
	$gotoPage = "issue_books";
$message = '';

	$memberId = trim($_POST['member']);
	$bookId = trim($_POST['book']);
	$issueDate = $_POST['issue_date'];

	$issueDate = changeDateToMysql($issueDate);

		try {
			$con->beginTransaction();
			
		$query = "INSERT INTO `books_issuance_history`(
		`member_id`, `book_id`, `issuance_date`) 
		VALUES ($memberId, $bookId, '$issueDate');";

		$stmtIssueDate = $con->prepare($query);
		$stmtIssueDate->execute();
		
		$con->commit();
	
	$message = 'Data hes been submitted successffully';
		
	} catch(PDOException $ex) {
		$con->rollback();
		$ex->getMessage();
		$ex->getTraceAsString();
		exit;
		}
	header("location:congratulation?goto_page=".$gotoPage."&message=".$message);
  exit;
}

$books = getAllBooks($con);
$members = getAllmembers($con);

$queryL50Issuance = "SELECT `m`.`member_name`, `m`.`contact_number`, 
`b`.`book_name`, `b`.`edition`, 
date_format(`bih`.`issuance_date`, '%d %b %Y') as `issuance_date` 
FROM `members` as `m`, 
`books` as `b`, 
`books_issuance_history` as `bih` 
WHERE `bih`.`member_id` = `m`.`id` AND 
`bih`.`book_id` = `b`.`id` 
ORDER BY `bih`.`id` DESC LIMIT 50;";
$stmtLatest = $con->prepare($queryL50Issuance);
$stmtLatest->execute();

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include './config/site-css.php';?>
</head>
<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php include './config/top-menu.php';?>

		<?php include './config/sidebar.php';?>

		<div class="content-wrapper">
			<!-- Main content -->
			<section class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="card card-primary">
								<div class="card-header">
									<h3 class="card-title">Issue Book</h3>
								</div>
								<form id="quickForm" method="post">
									<div class="card-body">
										<div class="row">
											<div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
												<label for="member">Select Member</label>
												<select required="required" id="member" name="member" class="form-control select2 is_valid">
													<?php echo $members;?>
												</select>

											</div>	
											<div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
												<label for="book">Select Book</label>
												<select id="book" name="book" required="required" 
												class="form-control select2 is_valid">
												<?php echo $books;?>
												
											</select>
										</div>
									</div>

									<div class="row">
										<div class="form-group 
										col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6">
										<label for="issue_date">Issue Date</label>
										 <div class="input-group date" id="issue_date" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" data-target="#issue_date" name="issue_date">
                        <div class="input-group-append" data-target="#issue_date" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>
									</div>
									
								<div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-12">
									<label for="">&nbsp;</label>
									<button type="submit" id="submit" 
									name="submit" class="btn btn-primary  btn-block">Save</button>
								</div>

							</div>

						</div>


					</form>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-primary">
					<div class="card-header">
						<h3 class="card-title">Issued Books (Last 50 Records)</h3>
					</div>
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 table-responsive">
						<table class="table table-bordered">
							<thead>
								<th>S.No</th>
								<th>Member</th>
								<th>Book</th>
								<th>Edition</th>
								<th>Issue Date</th>
							</thead>

							<tbody>
								<?php 
								$counter = 0;
								while($row = $stmtLatest->fetch(PDO::FETCH_ASSOC)) {
									$counter++;
								?>
								<tr>
									<td><?php echo $counter;?></td>
									<td><?php echo $row['member_name'].' '.$row['contact_number']; ?></td>
									<td><?php echo $row['book_name'];?></td>
									<td><?php echo $row['edition'];?></td>
									<td><?php echo $row['issuance_date'];?></td>
								</tr>
							<?php } ?>
							</tbody>
						</table>

					</div>	

				</div>
			</div>
		</div>
	</div>
</section>
</div>


<?php include './config/footer.php';?>
</div>

<?php 
 include './config/site-js.php';
?>



<script>
  $(function () {
    $('#issue_date').datetimepicker({
        format: 'L'
    });
  });

</script>

<script>
	$(document).ready(function() {
		showMenuSelected("#mnu_issue_return", "#mi_issue_book");
    });
</script>

</body>
</html>
