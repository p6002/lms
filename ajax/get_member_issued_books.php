<?php 
include '../config/connection.php';

  $memberId = $_GET['member_id'];
  $query = "SELECT 
`b`.`book_name`, `b`.`edition`, 
date_format(`bih`.`issuance_date`, '%d %b %Y') as `issuance_date`, 
(case when `bih`.`return_date` is null then '' else date_format(`bih`.`return_date`, '%d %b %Y') end) as `return_date` 
FROM `books` as `b`, 
`books_issuance_history` as `bih` 
WHERE `bih`.`member_id` = $memberId AND 
`bih`.`book_id` = `b`.`id` 
ORDER BY `bih`.`id` asc;";
$stmt = $con->prepare($query);
$stmt->execute();

$data = '';
$counter = 0;
while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
  $counter++;
  $data = $data.'<tr>';

  $data = $data.'<td>'.$counter.'</td>';
  $data = $data.'<td>'.$row['book_name'].'</td>';
  $data = $data.'<td>'.$row['edition'].'</td>';
  $data = $data.'<td>'.$row['issuance_date'].'</td>';
  $data = $data.'<td>'.$row['return_date'].'</td>';
  
  $data = $data.'</tr>';

}

echo $data;

?>