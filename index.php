<?php 
include './config/connection.php';

$message = '';

	if(isset($_POST['submit'])) {
		$userName = $_POST['user_name'];
		$password = $_POST['password'];

		$encryptedPassword = md5($password);
		$query = "SELECT * FROM `users` 
		WHERE `user_name`='$userName' and 
		`password`='$encryptedPassword' and 
		`is_active` = 1;";
		
		$stmt = $con->prepare($query);
		$stmt->execute();
		
		$numberOfRows = $stmt->rowCount();
		if($numberOfRows == 0) {
			$message = 'incorrect username or password.';
		} else {
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			$_SESSION['user_id'] = $row['id'];
			$_SESSION['user_name'] = $row['user_name'];
			$_SESSION['email'] = $row['email'];
			$_SESSION['display_name'] = $row['display_name'];
			$_SESSION['profile_picture'] = $row['profile_picture'];

			header("location:dashboard");
		}

	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include './config/site-css.php';?>

</head>
<body>
	<div class="wrapper">
		
<div class="col-lg-6 col-md-6 col-12 offset-lg-3 offset-md-3">
<section class="content-header">
				<div class="container-fluid">
					<div class="row mb-2">
						<div class="col-sm-6">
							<h1>LMS LOGIN</h1>
						</div>
					</div>
				</div>
			</section>

			<section class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="card card-primary">
								<div class="card-header">
									<h3 class="card-title">Please Login to Proceed!</h3>
								</div>
								<!-- /.card-header -->
								<!-- form start -->
								<form method="post">
									<div class="card-body">
										<div class="form-group">
											<label for="user_name">Username</label>
											<input type="text" name="user_name" class="form-control" id="user_name" required="required">
										</div>
										<div class="form-group">
											<label for="password">Password</label>
											<input type="password" name="password" class="form-control" id="password" required="required">
										</div>

										
									</div>
									<!-- /.card-body -->
									<div class="card-footer">
										<button type="submit" name="submit" class="btn btn-primary">Login</button>
										<?php 
											if($message != '') {
												echo '<p class="text-danger">'.$message.'</p>';
											}
										?>
									</div>
								</form>
							</div>
							<!-- /.card -->
						</div>
					</div>
				</div><!-- /.container-fluid -->
			</section>

</div>
			

			
		</div>



	<?php include './config/site-js.php';?>
	<script type="text/javascript">

	</script>
</body>
</html>
