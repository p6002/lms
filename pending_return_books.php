<?php
include './config/connection.php';
include "./common_service/common_functions.php";

$members = getAllmembers($con);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include './config/site-css.php';?>
</head>
<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php include './config/top-menu.php';?>

		<?php include './config/sidebar.php';?>

		<div class="content-wrapper">
			<!-- Main content -->
			<section class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="card card-primary">
								<div class="card-header">
									<h3 class="card-title">Issued Books</h3>
								</div>
									<div class="card-body">
										<div class="row">
											<div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
												<label for="member_issued">Select Member</label>
												<select id="member_issued" class="form-control select2">
													<?php echo $members;?>
												</select>

											</div>	
									
								<div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-12">
									<label for="">&nbsp;</label>
									<button type="button" id="search_issued" 
									 class="btn btn-primary  btn-block">Search</button>
								</div>

							</div>

							<div class="row">
								<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 table-responsive">
						<table class="table table-bordered">
							<thead>
								<th>S.No</th>
								<th>Book</th>
								<th>Edition</th>
								<th>Issue Date</th>
								<th>Return Date</th>
							</thead>

							<tbody id="member_issued_books">
							</tbody>
						</table>

					</div>	
							</div>

						</div>


				</div>
			</div>
		</div>
	</div>
</section>

<section class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="card card-primary">
								<div class="card-header">
									<h3 class="card-title">Pending Books</h3>
								</div>
									<div class="card-body">
										<div class="row">
											<div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
												<label for="member_pending">Select Member</label>
												<select id="member_pending" class="form-control select2">
													<?php echo $members;?>
												</select>

											</div>	
									
								<div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-12">
									<label for="">&nbsp;</label>
									<button type="button" id="search_pending" 
									 class="btn btn-primary  btn-block">Search</button>
								</div>

							</div>

							<div class="row">
								<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 table-responsive">
						<table class="table table-bordered">
							<thead>
								<th>S.No</th>
								<th>Book</th>
								<th>Edition</th>
								<th>Issue Date</th>
								<th>Action</th>
							</thead>

							<tbody id="member_pending_books">
							</tbody>
						</table>
					</div>	
							</div>
						</div>
				</div>
			</div>
		</div>
	</div>
</section>
</div>

<?php include './config/footer.php';?>
</div>
<?php 
 include './config/site-js.php';
?>
<script>
	$(document).ready(function() {
		showMenuSelected("#mnu_issue_return", "#mi_pending_return");

		$("#search_issued").click(function() {
			var memberId = $("#member_issued").val();
			if(memberId !== '') {
				$.ajax({

					  url: "ajax/get_member_issued_books.php",
					  type: "GET",
					  data: {
					  	"member_id":memberId,
					  },
					  cache: false,
					  async:false,
					  success: function(data) {
					     $("#member_issued_books").html(data);
					  },
					  error:function(jqXHR, exception) {
					  	console.log(jqXHR);
					  	console.log(exception);
					  }

				});
			} else {
				showCustomMessage('Please select a member.');
			}
		});

		$("#search_pending").click(function() {
			var memberId = $("#member_pending").val();

			if(memberId !== '') {
				$.ajax({
					  url: "ajax/get_member_pending_books.php",
					  type: "GET",
					  data: {
					  	"member_id":memberId,
					  },
					  cache: false,
					  async:false,
					  success: function(data) {
					     $("#member_pending_books").html(data);
					  },
					  error:function(jqXHR, exception) {
					  	console.log(jqXHR);
					  	console.log(exception);
					  }

				});
			} else {
				showCustomMessage('Please select a member.');
			}
		});
    });
</script>
</body>
</html>
