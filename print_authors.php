<?php

include("config/connection.php");

include('./pdflib/logics-builder-pdf.php');

if (!(isset($_SESSION['user_id']))) {
    header("location:index");
    exit;
}

$reportTitle = "Books";
$subTitle = "All Authors";


$pdf = new LB_PDF('P', false, $reportTitle, $subTitle, '');
$pdf->SetMargins(20, 20, 13);
$pdf->AliasNbPages();
$pdf->AddPage();

$pdf->SetWidths(array(15, 50, 50, 30));
$pdf->SetAligns(array('L', 'C', 'C', 'C'));

$titlesArray = array('S.No', 'Author Name', 'Email', 'Contact Number');
$pdf->AddTableHeader($titlesArray);
$pdf->SetAligns(array('L', 'L', 'L', 'L'));

$query = "SELECT * FROM `authors` order by `author_name` asc;";
$stmt = $con->prepare($query);
$stmt->execute();

$counter = 0;
while($r = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $counter++;
    $data = array($counter, 
    	$r['author_name'],
    	$r['email'],
    	$r['contact_number'],
    	
);

    $pdf->AddRow($data);
}

$pdf->Output('I', 'books.pdf');

?>