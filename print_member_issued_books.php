<?php

include("config/connection.php");

include('./pdflib/logics-builder-pdf.php');

if (!(isset($_SESSION['user_id']))) {
	header("location:index");
	exit;
}

$reportTitle = "Members";
$subTitle = "Issued Books";
$memberId = $_GET['member_id'];
$type = $_GET['type'];

$fileName = "member_issued_books.pdf";

$query = "SELECT 
`b`.`book_name`, `b`.`edition`, 
date_format(`bih`.`issuance_date`, '%d %b %Y') as `issuance_date`, 
(case when `bih`.`return_date` is null then '' else date_format(`bih`.`return_date`, '%d %b %Y') end) as `return_date` 
FROM `books` as `b`, 
`books_issuance_history` as `bih` 
WHERE `bih`.`member_id` = $memberId AND 
`bih`.`book_id` = `b`.`id` 
ORDER BY `bih`.`id` asc;";

$widths = array(15, 60, 35, 30, 30);
$CAligns = array('L', 'C', 'C', 'C', 'C');
$aligns = array('L', 'L', 'L', 'L', 'L');
$titlesArray = array('S.No', 'Book Name', 'Edition', 'Issued At',	'Reutrned At');

if($type == 'PENDING') {
	$widths = array(15, 60, 35, 30);
	$CAligns = array('L', 'C', 'C', 'C');
	$aligns = array('L', 'L', 'L', 'L');
$fileName = "member_pending_books.pdf";
$titlesArray = array('S.No', 'Book Name', 'Edition', 'Issued At');

	$subTitle = "Pending Books";
	$query = "SELECT `bih`.`id`, 
	`b`.`book_name`, `b`.`edition`, 
	date_format(`bih`.`issuance_date`, '%d %b %Y') as `issuance_date` 
	FROM `books` as `b`, 
	`books_issuance_history` as `bih` 
	WHERE `bih`.`member_id` = $memberId AND 
	`bih`.`book_id` = `b`.`id` and 
	`bih`.`return_date` is null 
	ORDER BY `bih`.`id` asc;";
}
$stmt = $con->prepare($query);
$stmt->execute();

$pdf = new LB_PDF('P', false, $reportTitle, $subTitle, '');
$pdf->SetMargins(20, 20, 13);
$pdf->AliasNbPages();
$pdf->AddPage();

$queryMember = "SELECT  `member_name`, `cnic_number`, 
`contact_number` 
FROM `members` WHERE `id`=$memberId;";
$stmtMember = $con->prepare($queryMember);
$stmtMember->execute();
$rowMember = $stmtMember->fetch(PDO::FETCH_ASSOC);
$pdf->SetWidths(array(40, 100));
$pdf->SetAligns(array('L', 'L'));
$data = array("Name", $rowMember['member_name']);
$pdf->AddRow($data, false);
$data = array("CNIC", $rowMember['cnic_number']);
$pdf->AddRow($data, false);
$data = array("Contact", $rowMember['contact_number']);
$pdf->AddRow($data, false);
$pdf->Ln();


$pdf->SetWidths($widths);
$pdf->SetAligns($CAligns);

$pdf->AddTableHeader($titlesArray);
$pdf->SetAligns($aligns);

$i = 0;
while($r = $stmt->fetch(PDO::FETCH_ASSOC)) {
	$i++;
	if($type == 'PENDING') {
		$data = array($i, $r['book_name'], $r['edition'], $r['issuance_date']);
	} else {
		$data = array($i, $r['book_name'], $r['edition'], $r['issuance_date'], $r['return_date']);
	}
	
	$pdf->AddRow($data);
}
$pdf->Output('I', $fileName);
?>