<?php

include("config/connection.php");

include('./pdflib/logics-builder-pdf.php');

if (!(isset($_SESSION['user_id']))) {
    header("location:index");
    exit;
}

$reportTitle = "Books";
$subTitle = "All Books";


$pdf = new LB_PDF('L', false, $reportTitle, $subTitle, '');
$pdf->SetMargins(13, 10, 13);
$pdf->AliasNbPages();
$pdf->AddPage();

$pdf->SetWidths(array(15, 50, 50, 30, 30, 30, 30, 30));
$pdf->SetAligns(array('L', 'C', 'C', 'C', 'C', 'C', 'C', 'C'));

$titlesArray = array('S.No', 'Book Name', 'Category', 'Author',	'Price', 'Pages', 'Edition', 'QTY');
$pdf->AddTableHeader($titlesArray);
$pdf->SetAligns(array('L', 'L', 'L', 'L', 'R', 'R', 'L', 'R'));

$query = "SELECT `b`.`id`, `b`.`book_name`, `b`.`number_of_page`, `b`.`purchase_price`, `b`.`author_id`, `b`.`edition`, 
`b`.`quantity`, `b`.`category_id`, `c`.`category`, 
`a`.`author_name` 
FROM `books` AS `b`, `categories` AS `c`, `authors` AS `a` 
WHERE `c`.`id` = `b`.`category_id` and 
`a`.`id` = `b`.`author_id` 
order by `c`.`category` asc;";
$stmt = $con->prepare($query);
$stmt->execute();

$counter = 0;
while($r = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $counter++;
    $data = array($counter, 
    	$r['book_name'],
    	$r['category'],
    	$r['author_name'],
    	$r['purchase_price'],
    	$r['number_of_page'],
    	$r['edition'],
    	$r['quantity']
);

    $pdf->AddRow($data);
}

$pdf->Output('I', 'books.pdf');

?>