<?php 
include './config/connection.php';
include "./common_service/common_functions.php";

if(isset($_POST['submit'])) {
  
  $gotoPage = "books";
  $message = "";
  
  $category_id = $_POST['category_name'];
  $author_id = $_POST['author_name'];
  $book_name = trim($_POST['book_name']);
  $number_of_pages = trim($_POST['number_of_page']);
  $purchase_price = trim($_POST['purchase_price']);
  $edition = trim($_POST['edition']);
  $quantity = trim($_POST['quantity']);

  $book_name = ucwords(strtolower($book_name));
  
  $book_name = replaceSingleQuote($book_name);
  
  $edition = replaceSingleQuote($edition);
  
  try {
    $con->beginTransaction();
    if($book_name != '' && $purchase_price > 0 && $number_of_pages > 0 && $edition != ''  && $quantity > 0 ) {

      $query = "INSERT INTO `books`(`book_name`, 
      `number_of_page`, `purchase_price`, `author_id`, 
      `edition`, `quantity`, `category_id`) 
      VALUES ('$book_name', $number_of_pages, $purchase_price, 
      $author_id, '$edition', $quantity, $category_id);";

      $stmt = $con->prepare($query);
      $stmt->execute();
     
      $message = 'Book has been registered successfully.';

    }
    $con->commit();

  } catch(PDOException $ex) {
    $con->rollback();
    echo $ex->getMessage();
    echo $ex->getTraceAsString();
    exit;
  }
  header("location:congratulation?goto_page=".$gotoPage."&message=".$message);
  exit;
}


$category = getAllcategory($con);
$author = getAllAuthor($con);

$query = "SELECT `b`.`id`, `b`.`book_name`, `b`.`number_of_page`, `b`.`purchase_price`, `b`.`author_id`, `b`.`edition`, 
`b`.`quantity`, `b`.`category_id`, `c`.`category`, 
`a`.`author_name` 
FROM `books` AS `b`, `categories` AS `c`, `authors` AS `a` 
WHERE `c`.`id` = `b`.`category_id` and 
`a`.`id` = `b`.`author_id` 
order by `c`.`category` asc;";
$stmt = $con->prepare($query);
$stmt->execute();

?>
<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>L-M-S | Books</title>
<?php
include "./config/site-css.php";

?>
</head>
<body class="hold-transition sidebar-mini">
  <div class="wrapper">
    <!-- Navbar -->
    <?php
    include "./config/top-menu.php";

  // <!-- /.navbar -->
    include "./config/sidebar.php";

    ?>
    <!-- Main Sidebar Container -->
    <div class="clearfix">&nbsp;</div>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

           <div class="row">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card ">

              <div class="card-header bg-primary text-white">Add New book
              </div>
              <div class="card-body">
                <form method="POST">
                  <div class="row">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                      <label for="category">Select Category</label>

                      <select  id="category"
                      name="category_name" required="required" class="form-control select2 is_valid">
                      <?php echo $category;?>

                    </select>
                  </div>

                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                    <label for="author_name">Select Author
                    </label>
                    <select  id="author_name" name="author_name" required="required" 
                    class="form-control select2 is_valid">
                      <?php echo $author;?>
                    </select> 
                  </div>


                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                    <label for="book_name">Book Name</label>
                    <input type="text" id="book_name" placeholder="Enter Book Name" name="book_name" required="required" class="form-control is_valid">
                  </div>

                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                    <label for="number_of_pages">Number Of Pages</label>
                    <input type="text" placeholder="Enter Number Of Pages" id="number_of_pages" name="number_of_page" required="required" class="form-control is_valid">
                  </div>


                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                    <label for="purchase_price">Purchase Price</label>
                    <input type="text" placeholder="Enter Purchase Price" required="required" id="purchase_price" 
                    name="purchase_price" class="form-control is_valid">
                  </div>
                  <div class="col-xl-3 col-lg-2 col-md-2 col-sm-6 col-12">
                    <label for="edition">Edition</label>
                    <input type="text" placeholder="Enter Edition" required="required" id="edition" 
                    name="edition" class="form-control is_valid">
                  </div>

                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                    <label for="qantity">Quantity</label>
                    <input type="text" placeholder="Enter Quantity" required="required" id="qantity" 
                    name="quantity" class="form-control is_valid">
                  </div>

                  <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-12">
                    <label for="">&nbsp;</label>
                    <button type="submit" id="submit" 
                    name="submit" class="btn btn-primary  btn-block">Save</button>
                  </div>
                </div>

              </form>
              
            </div>
          </div>
        </div>
      </div>

      <!-- /.row -->
    </div><!-- /.container-fluid -->
    </section>
    <section class="content">
      <div class="container-fluid">

           <div class="row">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card ">

              <div class="card-header bg-primary text-white">All Books
              </div>
              <div class="card-body">

                <div class="col-xl-1 col-lg-2 col-md-1 col-sm-3 col-12">
                    <a title="print all books in pdf" class="btn btn-success" target="_blank" href="print_books">
                      <i class="fa fa-print"></i>
                    </a>
                  </div>

                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 table-responsive">
                  <table class="table table-bordered" id="example1">
                    <thead>
                      <tr>
                        <th>S.No</th>
                        <th>Book Name</th>
                        <th>Category</th>
                        <th>Author</th>
                        <th>Price</th>
                        <th>Pages</th>
                        <th>Edition</th>
                        <th>QTY</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $couter = 0;
                      while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                        $couter++;
                      ?>
                      <tr>
                        <td><?php echo $couter;?></td>
                        <td><?php echo $row['book_name'];?></td>
                        <td><?php echo $row['category'];?></td>
                        <td><?php echo $row['author_name'];?></td>
                        <td><?php echo $row['purchase_price'];?></td>
                        <td><?php echo $row['number_of_page'];?></td>
                        <td><?php echo $row['edition'];?></td>
                        <td><?php echo $row['quantity'];?></td>
                        <td><a class="btn btn-primary btn-block" href="update_books?id=<?php echo $row['id'];?>">
                          <i  class="bg-primary fa fas fa-edit"></i>
                        </a></td>




                      </tr>
                      <?php
                    }

                      ?>
                    </tbody>
                  </table>
                  
                </div>
            </div>
          </div>
        </div>
      </div>

      <!-- /.row -->
    </div><!-- /.container-fluid -->
    </section>
    
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php
include "config/footer.php";
?>
</div>


<?php
include "./config/site-js.php"
?>

<script>
  $(document).ready(function() {
    showMenuSelected("#mnu_books", "#mi_books");
  });
</script>

</body>
</html>
