<?php
include './config/connection.php';

$id = $_GET['id'];

if (isset($_POST['submit'])) {
	$gotoPage = "authors";
	$message = '';

	$authorName = trim($_POST['author_name']);
	$contactNumber = trim($_POST['contact_number']);
	$authorEmail = trim($_POST['author_email']);

	$authorName = ucwords(strtolower($authorName));

	try {
		$con->beginTransaction();

		if ($authorName != '' && $authorEmail != '' 
			&& $contactNumber != '') {
			$query = "UPDATE `authors` SET `author_name` = '$authorName', `contact_number` = '$contactNumber', `email` = '$authorEmail' where `id` = $id;";

		$authorUpdate = $con->prepare($query);
		$authorUpdate->execute();
	}

	$con->commit();
	$message = 'Author has been updated successfully';

} catch(PDOException $ex) {
	$con->rollback();
	$ex->getMessage();
	$ex->getTraceAsString();
	exit;
}
header("location:congratulation?goto_page=".$gotoPage."&message=".$message);
exit;
}
$query = "SELECT * FROM `authors`where `id` = $id;";

$authorUpdate = $con->prepare($query);
$authorUpdate->execute();

$row = $authorUpdate->fetch(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include './config/site-css.php';?>
</head>
<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php include './config/top-menu.php';?>

		<?php include './config/sidebar.php';?>

		<div class="content-wrapper">
			<section class="content-header">
				<div class="container-fluid">
					<div class="row mb-2">
						<div class="col-sm-6">
							<h1>Authors</h1>
						</div>
					</div>
				</div>
			</section>

			<section class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="card card-primary">
								<div class="card-header">
									<h3 class="card-title">Update Author</h3>
								</div>
								<div class="card-body">
									<form id="quickForm" method="POST">
										<div class="row form-group">
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
						<label for="author_name">Author Name</label>
						<input type="text" name="author_name" class="form-control is_valid" required="required" id="author_name" placeholder="Enter author name" value="<?php echo $row['author_name'];?>">
						</div>
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
			<label for="author_email">Email</label>
			<input type="text" name="author_email" class="form-control is_valid" required="required" id="author_email" placeholder="Enter author email" value="<?php echo $row['email'];?>">
			</div>
			</div>
			<div class="row form-group">
			<div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
			<label for="contact_number">Contact Number</label>
			<input type="text" name="contact_number" class="form-control is_valid" required="required" id="contact_number" placeholder="Enter author phone number" value="<?php echo $row['contact_number'];?>">
			</div>

			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
			<label for="submit">&nbsp;</label>
			<button type="submit" name="submit" class="btn btn-success btn-block">Submit</button>
											</div>
										</div>
									</form>

								</div>
							</div>
							<!-- /.card -->
						</div>
					</div>
				</div>
			</section>
			<div class="clearfix">&nbsp;</div>

		</div>

		<?php include './config/footer.php';?>
	</div>

	<?php include './config/site-js.php';?>

</body>
</html>
