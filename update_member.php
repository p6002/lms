<?php 
	include './config/connection.php';
	
	$id = $_GET['id'];
	$query = "SELECT * FROM `members`
	WHERE `id` = $id;";
	$stmntUpdate = $con->prepare($query);
	$stmntUpdate->execute();
	$row = $stmntUpdate->FETCH(PDO::FETCH_ASSOC);

	if (isset($_POST['submit'])) {
		$gotoPage = "members";
		$message = "";

		$memberName = trim($_POST['member_name']);
		$cnic = trim($_POST['cnic_number']);
		$contactNumber = trim($_POST['contact_number']);
		$address = trim($_POST['address']);

		$memberName = ucwords(strtolower($memberName));
		$address = ucwords(strtolower($address));
		try {
			$con->beginTransaction();
			if ($memberName != '' && $cnic != '' && $contactNumber != '' && $address != '') {
				$query = "UPDATE `members` SET `member_name`='$memberName',`cnic_number`='$cnic',`contact_number`='$contactNumber',`address`='$address' WHERE `id`='$id';";
				$stmntUpdate = $con->prepare($query);
				$stmntUpdate->execute();
			}
			$con->commit();
			$message = 'Member has been Updated Successfully.';
			
		}
		catch(PDOException $e) {
			$con->rollback();
			echo $e->getMessage();
			echo $e->getTraceAsString();
			exit;
		}
		$gotoPage = 'members';
		header("location:congratulation?goto_page=".$gotoPage."&message=".$message);
		exit;
	}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include './config/site-css.php';?>

</head>
<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php include './config/top-menu.php';?>

		<?php include './config/sidebar.php';?>

		<div class="content-wrapper">
			<section class="content-header">
				<div class="container-fluid">
					<div class="row mb-2">
						<div class="col-sm-6">
							<h1>Page Title</h1>
						</div>
					</div>
				</div><!-- /.container-fluid -->
			</section>

			<!-- Main content -->
<section class="content">
<div class="container-fluid">
<div class="row">
<div class="col-md-12">
	<div class="card card-primary">
		<div class="card-header">
			<h3 class="card-title">Update Member</h3>
		</div>
	<div class="card-body">
		<form method="POST">
			<div class="row">
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12 ">
				<label>Member Name</label>
				<input type="text" value="<?php echo $row['member_name'];?>" name="member_name" required="required" class="form-control">		</div>
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12 ">
				<label>Cnic Number</label>
				<input type="text" value="<?php echo $row['cnic_number'];?>" name="cnic_number" required="required" class="form-control">		</div>
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12 ">
				<label>Contact Number</label>
				<input type="text" value="<?php echo $row['contact_number'];?>" name="contact_number" required="required" class="form-control">		</div>
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12 ">
				<label>Address</label>
				<input type="text" value="<?php echo $row['address'];?>" name="address" required="required" class="form-control">		</div>
				<div class="col-6"></div>
				<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-2 ">
					<div class="clearfix">&nbsp;</div>
					<button type="submit" name="submit" class="btn btn-success btn-block">Update</button>
				</div>
			</div>
		</form>
	</div>
	</div>
							
</div>
</div>
</div>
</section>
</div>


		<?php include './config/footer.php';?>
	</div>

	<?php include './config/site-js.php';?>
	<script type="text/javascript">
		var message = '<?php echo $message;?>';
    if(message !== '') {
      alert(message);
    }
	</script>
</body>
</html>
