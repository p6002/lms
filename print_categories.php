<?php

include("config/connection.php");

include('./pdflib/logics-builder-pdf.php');

if (!(isset($_SESSION['user_id']))) {
    header("location:index");
    exit;
}

$reportTitle = "Books";
$subTitle = "Categories ";


$pdf = new LB_PDF('P', false, $reportTitle, $subTitle, '');
$pdf->SetMargins(26, 10, 13);
$pdf->AliasNbPages();
$pdf->AddPage();

$pdf->SetWidths(array(13, 100));
$pdf->SetAligns(array('L', 'C'));

$titlesArray = array('S.No', 'Category Name');
$pdf->AddTableHeader($titlesArray);
$pdf->SetAligns(array('L', 'L'));

$query = "select * from `categories` order by `category` asc;";
$stmt = $con->prepare($query);
$stmt->execute();
$counter = 0;
while($r = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $counter++;
    $data = array($counter, $r['category']);

    $pdf->AddRow($data);
}
$pdf->Output('I', 'categories.pdf');

?>