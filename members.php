<?php 
	include './config/connection.php';
	include './common_service/common_functions.php';

	if (isset($_POST['submit'])) {
		$gotoPage = "members";
		$message = "";

		$memberName = trim($_POST['member_name']);
		$cnic = trim($_POST['cnic']);
		$contactNumber = trim($_POST['contact_number']);
		$address = trim($_POST['address']);

		

		$memberName = ucwords(strtolower($memberName));
		$address = ucwords(strtolower($address));

		try {
			$con->beginTransaction();
			
			if ($memberName != '' && $cnic != '' && $contactNumber != '' && $address != '') {
				$query = "INSERT INTO `members`(`member_name`, `cnic_number`, `contact_number`, `address`) VALUES('$memberName', '$cnic', '$contactNumber', '$address');";
				$stmntMembers = $con->prepare($query);
				$stmntMembers->execute();	
			}
			
			$con->commit();

			$message = 'Member has been Registered Successfully.';
		}
		catch(PDOException $e) {
			$con->rollback();
			echo $e->getMessage();
			echo $e->getTraceAsString();
			exit;
		}
		header("location:congratulation?goto_page=".$gotoPage."&message=".$message);
			exit;
	}
	$query = "SELECT * FROM `members`
	ORDER BY `member_name` ASC;";
	$stmntMembers = $con->prepare($query);
	$stmntMembers->execute();
	 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include './config/site-css.php';?>

</head>
<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php include './config/top-menu.php';?>

		<?php include './config/sidebar.php';?>

		<div class="content-wrapper">
			<section class="content-header">
				<div class="container-fluid">
					<div class="row mb-2">
						<div class="col-sm-6">
							<h1>Add New Member</h1>
						</div>
					</div>
				</div><!-- /.container-fluid -->
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="card card-primary">
								<div class="card-header">
									<h3 class="card-title">Add Member</h3>
								</div>
		<div class="card-body">
			<form class="form" method="POST">
				<div class="row">
					<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<div class="form-group">
							<label>Member Name</label>
							<input type="text" class="form-control is_valid" name="member_name" id="member_name" required="required">
						</div>
					</div>

					<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<div class="form-group">
							<label>Cnic</label>
							<input type="text" class="form-control is_valid" required="required" name="cnic">
						</div>
					</div>

					<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<div class="form-group">
							<label>Contact Number</label>
							<input type="text" class="form-control is_valid" required="required" name="contact_number">
						</div>
					</div>

					<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<div class="form-group">
							<label>Address</label>
							<input type="text" class="form-control is_valid" required="required" name="address">
						</div>
					</div>
					<div class="col-6"></div>
					<div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-xs-12">
						<div class="">&nbsp;</div>
						<button type="submit" name="submit" class="btn btn-success btn-block">Save</button>
					</div>
				</div>
				
			</form>
		</div>	
							</div>
							
						</div>
					</div>
				</div><!-- /.container-fluid -->
			</section>

			<div class="clearfix">&nbsp;</div>
			<section class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="card card-primary">
								<div class="card-header">
									<h3 class="card-title">All Members</h3>
								</div>
		<div class="card-body">
			<div class="card-body">
			<div class="table-responsive col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<table class="table table-bordered table-hover" id="example1">
						<thead>
							<tr>
								<th>S.Number</th>
								<th>Member Name</th>
								<th>Cnic</th>
								<th>Contact Number</th>
								<th>Address</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php  
								$counter = 0;
								while ($row = $stmntMembers->fetch(PDO::FETCH_ASSOC)) {
									$counter++;
							?>
					<tr>
						<td><?php echo $counter; ?></td>
						<td><?php echo $row['member_name']; ?></td>
						<td><?php echo $row['cnic_number']; ?></td>
						<td><?php echo $row['contact_number']; ?></td>
						<td><?php echo $row['address']; ?></td>
						<td>
							<a href="update_member?id=<?php echo $row['id'];?>" class="btn btn-success"><i class="fas fa-edit"></i></a>
						</td>
						</tr>
						<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>	
							</div>
							
						</div>
					</div>
				</div><!-- /.container-fluid -->
			</section>
		</div>


		<?php include './config/footer.php';?>
	</div>

	<?php include './config/site-js.php';?>
	
	<script>
  $(document).ready(function() {
    showMenuSelected("#mnu_settings", "#mi_members");
  });
	</script>
</body>
</html>
