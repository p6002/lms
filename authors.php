<?php
include './config/connection.php';

if (isset($_POST['submit'])) {
	$gotoPage = "authors";
	$message = '';

	$authorName = trim($_POST['author_name']);
	$authorEmail = trim($_POST['author_email']);
	$contactNumber = trim($_POST['contact_number']);

	$authorName = ucwords(strtolower($authorName));

	try {
		$con->beginTransaction();
		if ($authorName != '' && $authorEmail != '' 
			&& $contactNumber != '') {
			$query = "INSERT INTO `authors`(`author_name`, 
		`contact_number`, `email`) 
		VALUES ('$authorName',  '$contactNumber', '$authorEmail');";

		$stmntAuthors = $con->prepare($query);
		$stmntAuthors->execute();
	}

	$con->commit();
	$message = 'Author has been added successfully';

} catch(PDOException $ex) {
	$con->rollback();
	$ex->getMessage();
	$ex->getTraceAsString();
	exit;
}
header("location:congratulation?goto_page=".$gotoPage."&message=".$message);
exit;
}
$query = "SELECT * FROM `authors` order by `author_name` asc;";

$authors = $con->prepare($query);
$authors->execute();
?>


<!DOCTYPE html>
<html lang="en">
<head>
	<?php include './config/site-css.php';?>
</head>
<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php include './config/top-menu.php';?>

		<?php include './config/sidebar.php';?>

		<div class="content-wrapper">
			<section class="content-header">
				<div class="container-fluid">
					<div class="row mb-2">
						<div class="col-sm-6">
							<h1>Authors</h1>
						</div>
					</div>
				</div>
			</section>

			<section class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="card card-primary">
								<div class="card-header">
									<h3 class="card-title">Add New Author</h3>
								</div>
								<div class="card-body">
									<form id="quickForm" method="POST">
										<div class="row form-group">
											<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
												<label for="author_name">Author Name</label>
												<input type="text" name="author_name" class="form-control is_valid" required="required" id="author_name" placeholder="Enter author name">
											</div>
											<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
												<label for="author_email">Email</label>
												<input type="text" name="author_email" class="form-control is_valid" required="required" id="author_email" placeholder="Enter author email">
											</div>
										</div>
										<div class="row form-group">
											<div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
												<label for="contact_number">Contact Number</label>
												<input type="text" name="contact_number" class="form-control is_valid" required="required" id="contact_number" placeholder="Enter author phone number">
											</div>

											<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
												<label for="submit">&nbsp;</label>
												<button type="submit" name="submit" class="btn btn-success btn-block">Submit</button>
											</div>
										</div>
									</form>

								</div>
							</div>
							<!-- /.card -->
						</div>
					</div>
				</div>
			</section>
			<div class="clearfix">&nbsp;</div>

			<section class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="card card-primary">
								<div class="card-header">
									<h3 class="card-title">Add New Author</h3>
								</div>
								<div class="card-body">
									<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 table-responsive">
										<table id="example1" class="table table-bordered table-striped">
											<thead>
												<tr>
													<th>S.No</th>
													<th>Author Name</th>
													<th>Email</th>
													<th>Contact Number</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
	<?php
	$counter = 0;
	while ($row = $authors->fetch(PDO::FETCH_ASSOC)) {
	$counter++;

	?>
	<tr>
	<td><?php echo $counter; ?></td>
	<td><?php echo $row['author_name']; ?></td>
	<td><?php echo $row['email']; ?></td>
	<td><?php echo $row['contact_number']; ?></td>
	<td>
	<a class="btn btn-success" href="update_author?id=<?php echo $row['id']?>"><i class="fas fa-edit"></i></a>
	</td>
	</tr>
	<?php } ?>
											</tbody>
										</table>
										
									</div>
								</div>
							</div>
							<!-- /.card -->
						</div>
					</div>
				</div>
			</section>
		</div>

		<?php include './config/footer.php';?>
	</div>

	<?php include './config/site-js.php';?>

<script>
  $(document).ready(function() {
    showMenuSelected("#mnu_books", "#mi_authors");
  });
</script>
</body>
</html>
