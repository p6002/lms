<?php
include './config/connection.php';
include "./common_service/common_functions.php";

$members = getAllmembers($con);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include './config/site-css.php';?>
</head>
<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php include './config/top-menu.php';?>

		<?php include './config/sidebar.php';?>

		<div class="content-wrapper">
			<!-- Main content -->
			<section class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="card card-primary">
								<div class="card-header">
									<h3 class="card-title">Print Issued Books</h3>
								</div>
									<div class="card-body">
										<div class="row">
											<div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
												<label for="member_issued">Select Member</label>
												<select id="member_issued" class="form-control select2">
													<?php echo $members;?>
												</select>

											</div>	
									
								<div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-12">
									<label for="">Print Issued Books</label>
									<button type="button" id="search_issued" 
									 class="btn btn-success  btn-block"><i class="fas fa-print"></i></button>
								</div>

							</div>


						</div>


				</div>
			</div>
		</div>
	</div>
</section>

<section class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="card card-primary">
								<div class="card-header">
									<h3 class="card-title">Print Pending Books</h3>
								</div>
									<div class="card-body">
										<div class="row">
											<div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
												<label for="member_pending">Select Member</label>
												<select id="member_pending" class="form-control select2">
													<?php echo $members;?>
												</select>

											</div>	
									
								<div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-12">
									<label for="">&nbsp;</label>
									<button type="button" id="search_pending" 
									 class="btn btn-primary  btn-block">Search</button>
								</div>

							</div>

						</div>
				</div>
			</div>
		</div>
	</div>
</section>
</div>

<?php include './config/footer.php';?>
</div>
<?php 
 include './config/site-js.php';
?>
<script>
	$(document).ready(function() {
		showMenuSelected("#mnu_reports", "#mi_reports_members");

		$("#search_issued").click(function() {
			var memberId = $("#member_issued").val();
			if(memberId !== '') {
				var win = window.open("print_member_issued_books?member_id=" + memberId + "&type=ISSUED", "_blank");
				if(win) {
					win.focus();
				} else {
					showCustomMessage('Please allow popups for this site.');
				}
			} else {
				showCustomMessage('Please select a member.');
			}
		});

		$("#search_pending").click(function() {
			var memberId = $("#member_pending").val();

			if(memberId !== '') {
				var win = window.open("print_member_issued_books?member_id=" + memberId + "&type=PENDING", "_blank");
				if(win) {
					win.focus();
				} else {
					showCustomMessage('Please allow popups for this site.');
				}
			} else {
				showCustomMessage('Please select a member.');
			}
		});
    });
</script>
</body>
</html>
