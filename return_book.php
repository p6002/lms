<?php
include './config/connection.php';
include "./common_service/common_functions.php";


if (isset($_POST['submit'])) {
	$gotoPage = "pending_return_books";
	$message = '';

	$historyId = $_POST['history_id'];
	$returnDate = $_POST['return_date'];

	$returnDate = changeDateToMysql($returnDate);

		try {
			$con->beginTransaction();
			
		$query = "update `books_issuance_history` set 
		`return_date` = '$returnDate' 
		where `id` = $historyId;";

		$stmtReturn = $con->prepare($query);
		$stmtReturn->execute();
		
		$con->commit();
	
	$message = 'Selected book has been returned.';
		
	} catch(PDOException $ex) {
		$con->rollback();
		$ex->getMessage();
		$ex->getTraceAsString();
		exit;
		}
	header("location:congratulation?goto_page=".$gotoPage."&message=".$message);
  exit;
}

$historyId = $_GET['id'];
$query = "SELECT `m`.`member_name`, `bih`.`id`, 
`b`.`book_name`, `b`.`edition`, 
date_format(`bih`.`issuance_date`, '%d %b %Y') as `issuance_date` 
FROM `books` as `b`, `books_issuance_history` as `bih`, 
`members` as `m`  
WHERE `bih`.`id` = $historyId and 
`bih`.`member_id` = `m`.`id` AND 
`bih`.`book_id` = `b`.`id` and 
`bih`.`return_date` is null;";
$stmt = $con->prepare($query);
$stmt->execute();
$row = $stmt->fetch(PDO::FETCH_ASSOC);

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include './config/site-css.php';?>
</head>
<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php include './config/top-menu.php';?>

		<?php include './config/sidebar.php';?>

		<div class="content-wrapper">
			<!-- Main content -->
			<section class="content">
				<div class="container-fluid">
					
					<div class="row">
						<div class="col-md-12">
							<div class="card card-primary">
								<div class="card-header">
									<h3 class="card-title">Return Book</h3>
								</div>
								<form id="quickForm" method="post">
									<input type="hidden" name="history_id" value="<?php echo $historyId;?>">
									<div class="card-body">
										<div class="row">
						<div class="form-group 
										col-xl-2 col-lg-2 col-md-3 col-sm-4 col-12">
										Member Name
									</div>
									<div class="form-group 
										col-xl-10 col-lg-10 col-md-9 col-sm-8 col-12">
										<?php echo $row['member_name'];?>
									</div>

									<div class="form-group 
										col-xl-2 col-lg-2 col-md-3 col-sm-4 col-12">
										Book Title
									</div>
									<div class="form-group 
										col-xl-10 col-lg-10 col-md-9 col-sm-8 col-12">
										<?php echo $row['book_name'];?>
									</div>

									<div class="form-group 
										col-xl-2 col-lg-2 col-md-3 col-sm-4 col-12">
										Edition
									</div>
									<div class="form-group 
										col-xl-10 col-lg-10 col-md-9 col-sm-8 col-12">
										<?php echo $row['edition'];?>
									</div>

									<div class="form-group 
										col-xl-2 col-lg-2 col-md-3 col-sm-4 col-12">
										Issuance Date
									</div>
									<div class="form-group 
										col-xl-10 col-lg-10 col-md-9 col-sm-8 col-12">
										<?php echo $row['issuance_date'];?>
									</div>


					</div>
									<div class="row">
										<div class="form-group 
										col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6">
										<label for="return_date">Return Date</label>
										 <div class="input-group date" id="return_date" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" data-target="#return_date" name="return_date" required="required">
                        <div class="input-group-append" data-target="#return_date" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>
									</div>
									
								<div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-12">
									<label for="">&nbsp;</label>
									<button type="submit" id="submit" 
									name="submit" class="btn btn-primary  btn-block">Save</button>
								</div>

							</div>

						</div>


					</form>
				</div>
			</div>
		</div>
	</div>
</section>

</div>


<?php include './config/footer.php';?>
</div>

<?php 
 include './config/site-js.php';
?>

<script>
  $(function () {
    $('#return_date').datetimepicker({
        format: 'L'
    });
  });

</script>

<script>
	$(document).ready(function() {
		showMenuSelected("#mnu_issue_return", "#mi_issue_book");
    });
</script>

</body>
</html>
